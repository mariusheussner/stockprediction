

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import jdk.nashorn.internal.parser.JSONParser;

/**
 * Servlet implementation class StockPrediction
 */
@WebServlet("/StockPrediction")
public class StockPrediction extends HttpServlet {

	private static final long serialVersionUID = 1L;
    private static final double[] koeffizienten = {
    	-0.001111911,
    	24.59322548,
    	-3.103303909,
    	 0.0,
    	-0.070422836,
    	-0.079703569,
    	 0.002881327,
    	-0.164362237,
    	 0.135993719,
    	-0.026336908,
    	-0.10357134,
    	 0.105248414,
    	-10.50089836
    }; //-0.0000532947
    private static double[] currentFeatures = new double[koeffizienten.length-1];
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StockPrediction() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();	
			
		if (determineType(request).equals("human")){	
			out.write("Ausgabe des morgigen Aktienkurses für die Daimler AG Aktie lesbar für Menschen : " + determinePrediction()+"€");
			out.close();
		}
		if(determineType(request).equals("api")){
			out.write("{'stockprediction':'"+ determinePrediction() +"'}");
			out.close();
		}
	}
	
	private String determinePrediction() throws IOException{
		setFeatures();
		return calculateDaimlerStock();
	}

	public void setFeatures() throws IOException{
		//Features über APIs auslesen
		//Quandl
		currentFeatures[0] = getQuandl("YAHOO/INDEX_GDAXI"); //DAX
		currentFeatures[1] = getQuandl("CURRFX/EURUSD"); //USD
		currentFeatures[2] = getQuandl("CURRFX/EURCNY"); //CNY
		currentFeatures[3] = getQuandl("BCB/UDJIAD1"); //DOWJONES
		currentFeatures[4] = getQuandl("YAHOO/MI_BMW"); //BMW
		currentFeatures[5] = getQuandl("YAHOO/F_CON"); //Continental
		currentFeatures[6] = getQuandl("YAHOO/INDEX_STOXX50E"); //EUROSTOXX50
		currentFeatures[7] = getQuandl("YAHOO/F_VOW"); //VW
		currentFeatures[8] = getQuandl("NASDAQOMX/NQG3300EUR"); //NASDAQ Global EUR
		currentFeatures[9] = getQuandl("NASDAQOMX/NQEU3300"); //NASDAQ Europe Ind
		currentFeatures[10] = getQuandl("NASDAQOMX/NQG3300"); //NASDAQ Global Ind
		currentFeatures[11] = getQuandl("NASDAQOMX/NQEURO3300"); //NASDAQ Eurozone Ind

	}
	
	public double getQuandl(String QuandlCode){ 
		QuandlConnection r = new QuandlConnection("3jzUribyroSY6PTsUsBM");
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -7);
        String ago = dateFormat.format(cal.getTime());
        cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 0);
        String today =  dateFormat.format(cal.getTime());

		QDataset data1 = r.getDatasetBetweenDates(QuandlCode,ago,today);

		String close="";
		ArrayList<ArrayList<String>> data1Matrix = data1.getArrayMatrix();
		if(data1Matrix.get(0).size()>4){close = data1Matrix.get(0).get(4);}
		if(data1Matrix.get(0).size()==6){close = data1Matrix.get(0).get(1);}
		if(data1Matrix.get(0).size()==4){close = data1Matrix.get(0).get(1);}
		if(data1Matrix.get(0).size()==2){close = data1Matrix.get(0).get(1);}
        return Double.parseDouble(close);
	}
	
	private String calculateDaimlerStock(){
		System.out.println();
		for(int i = 0;i<currentFeatures.length;i++){
			System.out.println("Koeffizient Value: "+koeffizienten[i]+" - Feature Value: " +currentFeatures[i]);
		}
		System.out.println("Koeffizient Value: "+koeffizienten[koeffizienten.length-1]);
		double stockValue=0;
		for(int i=0;i<currentFeatures.length;i++){
			stockValue += koeffizienten[i]*currentFeatures[i];
		}
		stockValue = stockValue+koeffizienten[koeffizienten.length-1];
		DecimalFormat df = new DecimalFormat("#.##");
		String stockValueStr = df.format(stockValue);
		return stockValueStr;
	}
	
	private String determineType(HttpServletRequest request){
		Enumeration<String> parameterNames = request.getParameterNames();
		String paramName = parameterNames.nextElement();
		if (paramName.equals("type"))
		{
			String[] paramValues = request.getParameterValues(paramName);
			if(paramValues[0].equals("api")){
				return paramValues[0];	
			}
		}
		return "human";
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
